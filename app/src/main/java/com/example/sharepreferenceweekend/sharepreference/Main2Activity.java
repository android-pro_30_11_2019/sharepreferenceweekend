package com.example.sharepreferenceweekend.sharepreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.accessibility.AccessibilityManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharepreferenceweekend.R;
import com.example.sharepreferenceweekend.constant.USERCONSTANT;
import com.example.sharepreferenceweekend.model.User;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends AppCompatActivity {
    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = getSharedPreferences(USERCONSTANT.USERPREF,MODE_PRIVATE);
        tvUserName.setText(sharedPreferences.getString(USERCONSTANT.USERNAME,"N/A"));

        String strJSON = sharedPreferences.getString(USERCONSTANT.USEROBJ,"N/A");
        Gson gson = new Gson();
        User user = gson.fromJson(strJSON, User.class);
        if(user != null){
            Toast.makeText(this, user.getName(), Toast.LENGTH_SHORT).show();
        }
    }

}
