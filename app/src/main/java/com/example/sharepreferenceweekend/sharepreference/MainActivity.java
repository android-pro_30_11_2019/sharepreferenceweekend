package com.example.sharepreferenceweekend.sharepreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.example.sharepreferenceweekend.R;
import com.example.sharepreferenceweekend.constant.USERCONSTANT;
import com.example.sharepreferenceweekend.model.User;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.btnSave) Button btnSave;
    @BindView(R.id.btnGoActivity2)Button btnGoActivity2;
    @BindView(R.id.etUserName) EditText etUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        btnSave.setOnClickListener(view -> {
            SaveDataPreference();
            SaveDataJSONPreference();
        });
    }

    private void SaveDataJSONPreference() {
        //UserObject;
        User user = new User(1,"John Doe","M","07/01/1979");

        Gson gson = new Gson();
        String userJson = gson.toJson(user);
        Log.e("00000000",userJson);

        //writeSharePreference
        SharedPreferences sharedPreferences = getSharedPreferences(USERCONSTANT.USERPREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USERCONSTANT.USEROBJ, userJson);
        editor.apply();
    }

    private void SaveDataPreference() {
        //DECLARE
        SharedPreferences sharedPreferences = getSharedPreferences(USERCONSTANT.USERPREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USERCONSTANT.USERNAME, etUserName.getText().toString());
        editor.apply();
    }
    @OnClick(R.id.btnGoActivity2)
    public void onbtnGoActivity2Click(Button btnGoActivity2) {
        Intent intent = new Intent(this,Main2Activity.class);
        startActivity(intent);
    }
}
