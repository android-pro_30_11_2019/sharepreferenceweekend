package com.example.sharepreferenceweekend.constant;

public class USERCONSTANT {
    public static final String USERPREF = "USERPREF";
    public static final String USERNAME = "USERNAME";
    public static final String USEROBJ = "USEROBJ";
    public static final String TEXT_FILE_NAME = "file_name.txt";
}
