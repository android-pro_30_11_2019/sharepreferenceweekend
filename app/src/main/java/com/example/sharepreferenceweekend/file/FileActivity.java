package com.example.sharepreferenceweekend.file;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharepreferenceweekend.R;
import com.example.sharepreferenceweekend.constant.USERCONSTANT;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class FileActivity extends AppCompatActivity {

    @BindView(R.id.etContent)
    EditText etContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);

        ButterKnife.bind(this);

    }
    @OnClick(R.id.btnWritePrivate)
    public void onWritePrivateDataClick(Button btnWritePrivate){
        writePrivateData(etContent.getText().toString());
    }
    @OnClick(R.id.btnReadPrivate)
    public void onReadPrivateStorage(View view){
        readPrivateStorage();
    }
    @OnClick(R.id.btnWritePublic)
    public void onWritePublic(View v){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            int checked = ContextCompat.checkSelfPermission(FileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                    );
            if(checked == PackageManager.PERMISSION_GRANTED){
                WritePublicData(etContent.getText().toString());
            }else {
                //Request Runtime Permission;
                String[] permissions = new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                };
                requestPermissions(permissions,1);
            }
        }else
            WritePublicData(etContent.getText().toString());
    }
    @OnClick(R.id.btnReadPublicData)
    public void onReadPublicData(View view){
        ReadpublicData();
    }

    private void ReadpublicData() {
        FileInputStream fis = null;
        StringBuilder sb = new StringBuilder();
        try {
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            fis = new FileInputStream(new File(path,USERCONSTANT.TEXT_FILE_NAME));
            int c;
            while((c = fis.read()) != -1){
                //Convert to char
                char character = (char)c;
                sb.append(character);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                fis.close();
                Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1){
            if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                WritePublicData(etContent.getText().toString());
            }else{
                Toast.makeText(this, "Permision Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void WritePublicData(String str) {
        FileOutputStream fos = null;
        try {
            File path  = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            fos = new FileOutputStream(new File(path,USERCONSTANT.TEXT_FILE_NAME));
            fos.write(str.getBytes());
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void readPrivateStorage() {
        FileInputStream fis = null;
        StringBuilder sb = new StringBuilder();
        try {
            fis = openFileInput(USERCONSTANT.TEXT_FILE_NAME);
            int c;
            while((c = fis.read()) != -1){
                //Convert to char
                char character = (char)c;
                sb.append(character);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                fis.close();
                Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void writePrivateData(String str) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(USERCONSTANT.TEXT_FILE_NAME, MODE_PRIVATE);
            fos.write(str.getBytes());
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
