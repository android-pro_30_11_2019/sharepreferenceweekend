package com.example.sharepreferenceweekend.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {
    private static DbHelper dbHelper;

    public static DbHelper getInstance(Context context){
        if(dbHelper == null){
            dbHelper = new DbHelper(context);
        }
        return dbHelper;
    }
    private DbHelper(@Nullable Context context) {
        super(context, DBConstant.DB_NAME, null, DBConstant.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBConstant.CREATE_TBARTICLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
