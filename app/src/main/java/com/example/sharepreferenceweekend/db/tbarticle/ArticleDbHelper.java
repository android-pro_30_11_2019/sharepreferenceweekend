package com.example.sharepreferenceweekend.db.tbarticle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.sharepreferenceweekend.db.DBConstant;
import com.example.sharepreferenceweekend.db.DbHelper;

import java.util.ArrayList;
import java.util.List;

public class ArticleDbHelper {
    private DbHelper dbHelper;
    private static ArticleDbHelper articleDbHelper;
    private SQLiteDatabase db;

    private ArticleDbHelper(Context context) {
        dbHelper = DbHelper.getInstance(context);
    }
    public static ArticleDbHelper getInstance(Context context){
        if(articleDbHelper == null){
            articleDbHelper = new ArticleDbHelper(context);
        }
        return articleDbHelper;
    }
    //Insert;
    public boolean insert(Article article){
        db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBConstant.TITLE, article.getTitle());
        values.put(DBConstant.DESCRIPTION, article.getDescription());

        long result = db.insert(DBConstant.TBARTICLE,null,values);
        if(result>0){
            return true;
        }
        return false;
    }
    public List<Article> findAll(){
        List<Article> articleList = new ArrayList<>();
        db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query(DBConstant.TBARTICLE,
                null,null,null,
                null,null,null);

        if(cursor !=null){
            while (cursor.moveToNext()){
                int id = cursor.getInt(cursor.getColumnIndex(DBConstant.ID));
                String title = cursor.getString(cursor.getColumnIndex(DBConstant.TITLE));
                String description = cursor.getString(cursor.getColumnIndex(DBConstant.DESCRIPTION));

                articleList.add(new Article(id,title,description));//add new article to list;
            }
        }
        return articleList;
    }
    public boolean delete(int id){
        db = dbHelper.getWritableDatabase();
        int result = db.delete(
                DBConstant.TBARTICLE,
                " _id = ?",
                new String[]{String.valueOf(id)}
                );
        if(result>0){
            return true;
        }
        return false;
    }
    public boolean update(Article article){
        db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBConstant.TITLE, article.getTitle());
        values.put(DBConstant.DESCRIPTION, article.getDescription());

        int result = db.update(
                DBConstant.TBARTICLE,
                values,
                " _id = ?",
                new String[]{String.valueOf(article.getId())}
                );

        if(result>0){
            return true;
        }
        return false;
    }
}
