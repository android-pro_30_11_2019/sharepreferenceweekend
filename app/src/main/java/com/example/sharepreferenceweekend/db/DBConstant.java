package com.example.sharepreferenceweekend.db;

import android.provider.BaseColumns;

public final class DBConstant {
    //Database
    public static final String DB_NAME = "article_management.db";
    public static final int DB_VERSION = 1;


    //Table Article
    public static final String TBARTICLE = "tbarticle";
    public static final String ID = "_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";

    //Query Create Table;s
    public static final String CREATE_TBARTICLE_SQL = "CREATE TABLE "+ TBARTICLE + " ( "
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TITLE + " TEXT NOT NULL, "+
            DESCRIPTION + " TEXT NOT NULL)";
}
