package com.example.sharepreferenceweekend.db;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharepreferenceweekend.R;
import com.example.sharepreferenceweekend.db.tbarticle.Article;
import com.example.sharepreferenceweekend.db.tbarticle.ArticleDbHelper;

import java.util.List;

public class DbActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "000000";
    private EditText etId,etTitle,etDescription;
    private Button btnSave,btnFindAll,btnDelete, btnUpdate;
    private ArticleDbHelper articleDbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);

        //init view;
        etId = findViewById(R.id.etId);
        etTitle = findViewById(R.id.etTitle);
        etDescription = findViewById(R.id.etDescription);
        btnSave = findViewById(R.id.btnSave);
        btnFindAll = findViewById(R.id.btnFindAll);
        btnDelete = findViewById(R.id.btnDelete);
        btnUpdate = findViewById(R.id.btnUpdate);

        //Instance ArticleDbHelper
        articleDbHelper = ArticleDbHelper.getInstance(this);
        //event
        btnSave.setOnClickListener(this);
        btnFindAll.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSave:
                InsertArticle();
                break;
            case R.id.btnFindAll:
                FindAll();
                break;
            case R.id.btnDelete:
                int id = Integer.parseInt(etId.getText().toString());
                delete(id);
                break;
            case R.id.btnUpdate:

                Article article = new Article();
                article.setId(Integer.parseInt(etId.getText().toString()));
                article.setTitle(etTitle.getText().toString());
                article.setDescription(etDescription.getText().toString());

                update(article);
                break;
        }
    }

    private void update(Article article) {
        if(articleDbHelper.update(article)){
            Toast.makeText(this, "Record was updated", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, "Record updated failed", Toast.LENGTH_SHORT).show();
    }

    private void delete(int id) {
        if(articleDbHelper.delete(id)){
            Toast.makeText(this, "Record was deleted", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, "Record delete failed", Toast.LENGTH_SHORT).show();
    }

    private void FindAll() {

        List<Article>  listarticle = articleDbHelper.findAll();

        Log.e(TAG,"Data");
        for(Article article : listarticle){
            Log.e(TAG, article.toString());
        }
    }

    private void InsertArticle() {
        Article article = new Article();
        article.setTitle(etTitle.getText().toString());
        article.setDescription(etDescription.getText().toString());

        //Insert;
        if(articleDbHelper.insert(article)){
            Toast.makeText(this, "Your new article was save successfull!", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
    }
}
