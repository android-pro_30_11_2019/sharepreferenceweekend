package com.example.sharepreferenceweekend.model;

public class User {
    private int id;
    private String name;
    private String gender;
    private String dateofBirth;

    public User(int id, String name, String gender, String dateofBirth) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.dateofBirth = dateofBirth;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateofBirth() {
        return dateofBirth;
    }

    public void setDateofBirth(String dateofBirth) {
        this.dateofBirth = dateofBirth;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", dateofBirth='" + dateofBirth + '\'' +
                '}';
    }
}
